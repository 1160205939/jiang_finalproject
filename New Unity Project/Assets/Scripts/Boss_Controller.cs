﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_Controller : MonoBehaviour
{
    public LayerMask enemyMask;
    Rigidbody2D rd;
    Transform trans;
    public float FireRate;
    public int health;
    public bool isDead;

    public GameObject projectiles;
    public GameObject slap;
    public Transform front;
    public Transform poslap;

    private PlayerController playerController;
    private void Awake()
    {
        // playerController = GetComponent<PlayerController>();
    }
    // Use this for initialization
    void Start()
    {
        trans = this.transform;
        rd = this.GetComponent<Rigidbody2D>();

        GameObject playerControllerObject = GameObject.FindWithTag("Player");
        if (playerControllerObject != null)
        {
            playerController = playerControllerObject.GetComponent<PlayerController>();
        }
        if (playerController == null)
        {
            Debug.Log("Cannot find 'PlayerController' script");
        }

        InvokeRepeating("Fire", 2.0f, 2.5f); //Destroy the projectile in 0.75 sec
        InvokeRepeating("Slap", 3f, 4f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
    }
    public void Hurt(int dmg)
    {
        health -= dmg;

        if (health <= 0)
        {
            isDead = true;
        }
        if (isDead)
        {
            Death();
            playerController.score += 50;
        }
    }
    void Death()
    {
        Destroy(gameObject);
        Debug.Log("1123");
        playerController.win = "Congrats, You Win!"; 
    }

    void Fire()
    {
        Instantiate(projectiles, front.position, front.rotation);
    }

    void Slap()
    {
        Instantiate(slap, poslap.position, poslap.rotation);
    }

}
