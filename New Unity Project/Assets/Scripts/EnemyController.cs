﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {
    public LayerMask enemyMask;
    public float speed;
    Rigidbody2D rd;
    Transform trans;
    public int health;
    public bool isDead = false;
    float width;
    public AudioSource die;

    private PlayerController playerController;
    private void Awake()
    {
       // playerController = GetComponent<PlayerController>();
    }
    // Use this for initialization
    void Start () {
        trans = this.transform;
        rd = this.GetComponent<Rigidbody2D>();
        width = this.GetComponent<SpriteRenderer>().bounds.extents.x;

        die = GetComponent<AudioSource>();

        GameObject playerControllerObject = GameObject.FindWithTag("Player");
        if (playerControllerObject != null)
        {
            playerController = playerControllerObject.GetComponent<PlayerController>();
        }
        if (playerController == null)
        {
            Debug.Log("Cannot find 'PlayerController' script");
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        //check if there's a ground in front of enenmy before moving forward
        Vector2 LinecastPos = trans.position - trans.right * width;

        Debug.DrawLine(LinecastPos, LinecastPos + Vector2.down);
        bool grounded = Physics2D.Linecast(LinecastPos, LinecastPos + Vector2.down);

        if (!grounded) {
            speed = -1 * speed;
            Vector3 currRot = trans.eulerAngles;
            currRot.y += 180;
            trans.eulerAngles = currRot;
        }

        //always moving forward
        Vector2 vel = rd.velocity;
        vel.x = speed;
        rd.velocity = vel;
	}
    public void Hurt(int dmg) {
        health -= dmg;
        GetComponent<AudioSource>().Play();
        if (health <= 0)
        {
            isDead = true;
        }
        if (isDead)
        {
            Death();
        }
    }
    void Death()
    {
        Destroy(gameObject);
        playerController.Magic += 1;
        playerController.score += 10;
    }
}
