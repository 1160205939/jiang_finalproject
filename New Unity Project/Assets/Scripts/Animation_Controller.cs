﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation_Controller : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        Invoke("DestroyMe", 0.2f);//Destroy the projectile in 0.75 sec
    }

    void DestroyMe()
    {
        if (gameObject != null)
        {
            Destroy(gameObject);
        }
    }
}
