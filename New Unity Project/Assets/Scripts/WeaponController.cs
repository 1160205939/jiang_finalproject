﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour {
    private Rigidbody2D rb2d;
    private Animator anim;
    public GameObject MagicExplode;

    // Use this for initialization
    void Awake() {
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();

    }

    private void Start()
    {
    }

    // Update is called once per frame
    void Update () {
        Invoke("DestroyMe", 0.75f);//Destroy the projectile in 0.75 sec
	}
    void OnCollisionEnter2D(Collision2D collision)
    {

        EnemyController enemy = collision.collider.GetComponent<EnemyController>();
        EnemyController_Ranged enemy_ranged = collision.collider.GetComponent<EnemyController_Ranged>();

        if (enemy != null)
        {
            enemy.Hurt(60);
            Destroy(gameObject);
            Instantiate(MagicExplode, transform.position, transform.rotation);
        }
        if (enemy_ranged != null)
        {
            enemy_ranged.Hurt(60);
            Destroy(gameObject);
            Instantiate(MagicExplode, transform.position, transform.rotation);
        }
    }
    void DestroyMe()
    {
        Destroy(gameObject);
    }
}
