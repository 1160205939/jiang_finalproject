﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public static PlayerController instance;

    [HideInInspector] public bool jump = false;
    public bool facingRight;
    public float maxSpeed = 7f;
    public float jumpForce = 1000f;
    public int score;
    public string win;
    public Transform groundCheck;
    public Transform front;
    public Transform DeathPoint;
    public GameObject throwing;
    public GameObject Tomb;
    public Text deathText;
    public Text ScoreText;
    public Text WinText;
    public Button restartButton;

    private bool grounded = false;
    public Animator anim;
    private Rigidbody2D rb2d;
    Renderer rend;

    public int health;
    public int numberofHearts;
    public Image[] hearts;
    public Sprite FullHearts;
    public Sprite EmptyHearts;

    public int Magic = 3;
    public int numberofMana;
    public Image[] mana;
    public Sprite fullMana;
    public Sprite emptyMana;

    public float invicibleTimeAfterHurt = 2;
    Collider2D[] myColls;

    public AudioSource hurt;

    // Use this for initialization
    void Awake()
    {
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        rend = GetComponent<Renderer>();
    }
    void Start()
    {
        instance = this;
        myColls = GetComponents<Collider2D>();

        deathText.text = "";
        ScoreText.text = "";
        WinText.text = "";
        restartButton.gameObject.SetActive(false);

        hurt = GetComponent<AudioSource>();
    }

    void PlayerControl()
    {
        float moveX = Input.GetAxis("Horizontal");
        //        anim.SetFloat("Speed", Mathf.Abs(h));
        if (moveX < 0.0f && facingRight == false) {
            Flip();
        }
        if (moveX > 0.0f && facingRight) {
            Flip();
        }

        rb2d.velocity = new Vector2(moveX * maxSpeed, rb2d.velocity.y);

        if (jump)
        {
//            anim.SetTrigger("Jump");
            rb2d.AddForce(new Vector2(0f, jumpForce));
            jump = false;
        }

        //Player presses left Ctrl and throw a item
        if (Input.GetKeyDown(KeyCode.LeftControl) && Magic >= 1) {
            Fire();
            Magic--;
        }

        if (Magic <= 0) {
            Magic = 0;
        }
        if (Magic >= 3) {
            Magic = 3;
        }
    }
    // Update is called once per frame
    void Update()
    {
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        if (grounded)
        {
            anim.SetBool("IsJumping", false);
        }
        else {
            anim.SetBool("IsJumping", true);
        }
        if (Input.GetKeyDown(KeyCode.LeftAlt) && grounded)
        {
            jump = true;
        }
        PlayerControl();

        anim.SetFloat("Speed", Mathf.Abs(Input.GetAxis("Horizontal")));

        WinText.text = "" + win;

        //heart system
        for (int i= 0; i < hearts.Length; i++)
        {
            
            if (i<health)
            {
                hearts[i].sprite = FullHearts;
            }
            else
            {
                hearts[i].sprite = EmptyHearts;
            }

            if (i < numberofHearts)
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }

            ScoreText.text = "Score: " + score;
        }

        //magic system
        for (int i = 0; i < mana.Length; i++)
        {

            if (i < Magic)
            {
                mana[i].sprite = fullMana;
            }
            else
            {
                mana[i].sprite = emptyMana;
            }

            if (i < numberofMana)
            {
                mana[i].enabled = true;
            }
            else
            {
                mana[i].enabled = false;
            }
        }

        //falling system
        if (gameObject.transform.position.y <= -125.5f) {
            Death();
        }
    }
    public void Death()
    {
        Instantiate(Tomb, DeathPoint.position, DeathPoint.rotation);
        gameObject.SetActive(false);
        deathText.text = "GAME OVER";
        restartButton.gameObject.SetActive(true);
    }
    private void OnCllisionEnter2D (Collider2D collision)
    {
        if (collision.gameObject.name == "Enemy") {
            Destroy(collision.gameObject);
        }
    }

    void Flip()
    {
        facingRight = !facingRight;

        transform.Rotate(0f, 180f, 0f);
    }
    public void Hurt() {
        health -= 1;
        if (health <= 0) {
            Death();
        }
        else
        {
            TriggerHurt(invicibleTimeAfterHurt);
            rb2d.AddForce(new Vector2(-50f, 200f));
            hurt.Play();
        }
    }
    void Fire() {
        Instantiate(throwing, front.position, front.rotation);
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        EnemyController enemy = collision.collider.GetComponent<EnemyController>();
        if (enemy != null) {
            print(transform.position + " bunny: " + collision.gameObject.transform.position.y);
                if (transform.position.y > collision.gameObject.transform.position.y + 2)
                {
                    Vector2 velocity = rb2d.velocity;
                    rb2d.AddForce(new Vector2(0f, 300f));
                    enemy.Hurt(150);
                }
                else
                {
                    Hurt();
                }
                //}
        }

        EnemyController_Ranged ranged_enemy = collision.collider.GetComponent<EnemyController_Ranged>();
        if (ranged_enemy != null)
        {
            print(transform.position + " flower: " + collision.gameObject.transform.position.y);
            if (transform.position.y > collision.gameObject.transform.position.y + 1.8)
            {
                Vector2 velocity = rb2d.velocity;
                rb2d.AddForce(new Vector2(0f, 300f));
                ranged_enemy.Hurt(150);
            }
            else
            {
                Hurt();
            }
            //}
        }

        Boss_Controller boss_Controller = collision.collider.GetComponent<Boss_Controller>();
        if (boss_Controller != null)
        {

            foreach (ContactPoint2D point in collision.contacts)
            {
                Debug.Log(point.normal);
                Debug.DrawLine(point.point, point.point + point.normal, Color.red, 10);
                if (point.normal.y >= 0.9f)
                {
                    Vector2 velocity = rb2d.velocity;
                    rb2d.AddForce(new Vector2(0f, 300f));
                    boss_Controller.Hurt(150);
                }
                else
                {
                    Hurt();
                }
            }
        }
    }

    public void TriggerHurt(float hurtTime)
    {
        StartCoroutine(HurtBlinker(hurtTime));
    }
    IEnumerator HurtBlinker(float hurtTime)
    {
        //ignore the collsion between enemy
        int enemyLayer = LayerMask.NameToLayer("Enemy");
        int playerLayer = LayerMask.NameToLayer("Player");
        Physics2D.IgnoreLayerCollision(enemyLayer, playerLayer);
        foreach (Collider2D collider in instance.myColls)
        {
            collider.enabled = false;
            collider.enabled = true;
        }
        //start looping hurtanimation
        anim.SetLayerWeight(1, 1);
        //wait for hurt to end
        yield return new WaitForSeconds(hurtTime);
        //stop hurting animation and re-enable colission
        Physics2D.IgnoreLayerCollision(enemyLayer, playerLayer,false);
        anim.SetLayerWeight(1, 0);
        //rend.material.color = new Color(255f, 255f, 255f,255f);
    }

}