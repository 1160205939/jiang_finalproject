﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectiles : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private Animator anim;
    public GameObject Explode;
    public PlayerController playerController;
    public EnemyController_Ranged enemyController_Ranged;
    public Boss_Controller bossController;

    // Use this for initialization
    void Awake()
    {
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();

    }
    private void Start()
    {
        GameObject playerControllerObject = GameObject.FindWithTag("Player");
        if (playerControllerObject != null)
        {
            playerController = playerControllerObject.GetComponent<PlayerController>();
        }
        if (playerController == null)
        {
            Debug.Log("Cannot find 'PlayerController' script");
        }


    }

    // Update is called once per frame
    void Update()
    {
        Invoke("DestroyMe", 2f);//Destroy the projectile in flying_time sec
    }
    void OnCollisionEnter2D(Collision2D collision)
    {

        PlayerController player = collision.collider.GetComponent<PlayerController>();
        if (player != null)
        {
            player.Hurt();
            Destroy(gameObject);
            Instantiate(Explode, transform.position, transform.rotation);
        }
    }
    void DestroyMe()
    {
        Destroy(gameObject);
    }
}
