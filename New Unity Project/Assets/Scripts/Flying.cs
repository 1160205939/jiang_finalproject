﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flying : MonoBehaviour
{
    public float speed;
    public Rigidbody2D rd2d;
    // Use this for initialization
    void Start()
    {
        rd2d.velocity = transform.right * speed;
    }

    // Update is called once per frame
    void FixedUpdate() { }

}
